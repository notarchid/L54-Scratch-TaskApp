# Laracasts - Laravel 5.4 From Scratch - Task App
This is a Laracasts - Laravel 5.4 From Scratch First Project, building a Task App to learn about the basics of Laravel. The course covered about Laravel installations, basic routing, setting up laravel valet, setting up database, passing data to views, working with query builder, eloquent, controllers and route model binding.
