<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function index()
    {
    	$name = 'World';

		$tasks = Task::all();

		return view('tasks.index', compact('name', 'tasks'));
    }

    public function show(Task $task)
    {
    	$name = 'World';

		return view('tasks.show', compact('name', 'task'));
    }
}
